# Softventas Docs 
![Softventas Docs](https://gitlab.com/read424/softventas/docs/workflows/Softventas%20Docs/badge.svg)

Documentación de Softventas - Ventas Cafeteria.

***
Programa de ventas donde permitirá:
* crear, actualizar categorias de los articulos
* crear, actualizar articulos para la venta
* crear ventas

## Instalación
***
Una pequeña introducción como instalar api-rest
```
$ git clone https://gitlab.com/read424/softventas.git
$ cd ../softventas
$ npm install
```

## License

Softventas is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).