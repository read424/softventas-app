(function() {
    'use strict';
    angular.module('Gentelella.authService', [])
        .factory('AuthenticationService', [ '$localStorage', 'PermPermissionStore', function($localStorage, PermPermissionStore){
            var service={};
            $localStorage.$default({loggedIn: false,isAdministracion:false, isDirector:false,isProduccion:false,isAdmin:false,isAsistente:false,  isJefe:false,isSupervisor:false,id_usuario:false, username:false, empleado:'', id_area:'', cargo:'', id_rol:'', id_tienda:'',token:'', namerol:'', states:[], infoUser:{}});

            service.setNotification=function(){
                $localStorage.notification_array=[];
            };

            service.setStates=function(statepermission){
                $localStorage.states=statepermission;
            };

            service.setArea=function(area){
                $localStorage.id_area=area;
            };

            service.getArea=function(){
                return $localStorage.id_area;
            };

            service.getStatesusers=function(){
                return $localStorage.states;
            };

            service.setLoggedIn=function(login){
                $localStorage.loggedIn= JSON.stringify(login);
            };

            service.isLoggedIn=function(){
                return JSON.parse($localStorage.loggedIn);
            };

            service.setEmpleado=function(name){
                $localStorage.empleado=name;
            };

            service.getEmpleado=function(){
                return $localStorage.empleado;
            };

            service.setCargo=function(cargo){
                $localStorage.cargo=cargo;
            };

            service.getCargo=function(){
                return $localStorage.cargo;
            };

            service.setRol=function(name){
                $localStorage.namerol=name;
            };

            service.getRol=function(){
                return $localStorage.namerol;
            };

            service.setProduccion=function(isProduccion){
                $localStorage.isProduccion= JSON.stringify(isProduccion);
            };

            service.isProduccion=function(){
                var uservalid=$localStorage.namerol=="Produccion";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.setDirector=function(isDirector){
                $localStorage.isDirector= JSON.stringify(isDirector);
            };

            service.isDirector=function(){
                var uservalid=$localStorage.namerol=="Director";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.isAdmin=function(){
                var uservalid=$localStorage.namerol=="Administrador";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.setAdmin=function(isadmin){
                $localStorage.isAdmin= JSON.stringify(isadmin);
            };

            service.setSupervisor=function(isSupervisor){
                $localStorage.isSupervisor= JSON.stringify(isSupervisor);
            };

            service.isSupervisor=function(){
                var uservalid=$localStorage.namerol=="Supervisor";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.setAsistente=function(isAsistente){
                $localStorage.isAsistente= JSON.stringify(isAsistente);
            };

            service.isAsistente=function(){
                var uservalid=$localStorage.namerol=="Asistente";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.setJefe=function(isJefe){
                $localStorage.isJefe= JSON.stringify(isJefe);
            };

            service.isJefe=function(){
                var uservalid=$localStorage.namerol=="Jefe";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.setAdministracion=function(isAdministracion){
                $localStorage.isAdministracion= JSON.stringify(isAdministracion);
            };

            service.isAdministracion=function(){
                var uservalid=$localStorage.namerol=="Administracion";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };

            service.validAdmin=function(){
                var uservalid=$localStorage.id_rol==1;
                return (uservalid && $localStorage.loggedIn);
            };

            service.setUser = function(user){
                $localStorage.username=user;
            };

            service.getUser=function(){
                return $localStorage.username;
            };

            service.setID = function(id_usuario){
                $localStorage.id_usuario=id_usuario;
            };

            service.setAnioFiscal=function(infoAniaFiscal){
                $localStorage.anio_fiscal= infoAniaFiscal;
            };

            service.getID=function(){
                return $localStorage.id_usuario;
            };
    
            service.setToken = function(token) {
                $localStorage.token=token;
            };
    
            service.getToken = function() {
                return $localStorage.token;
            };
    
            service.signOut = function() {
                PermPermissionStore.clearStore();
                $localStorage.$reset();
            };

            service.loadData=function(data){
                _.extend($localStorage, data);
            };

            service.setPermissions=function(a_permissions){
                _.extend($localStorage, {
                    permissions: a_permissions
                });
            };

            service.getPermissions=function(){
                return $localStorage.permissions || [];
            };

            service.setEnableGuiaElectronica=function(isElectronic){
                _.extend($localStorage, { guia_electronica: isElectronic });
            };

            service.getEnableGuiaElectronica=function(){
                return $localStorage.guia_electronica || false;
            };

            service.getAnioFiscal=function(){
                return $localStorage.anio_fiscal || false;
            };

            return service;
        }]);
})();
