(function(){
    'use strict';
    angular.module('Gentelella',[
        'ngAnimate',
        'ngTouch',
        'ngStorage',
        'ngSanitize',
        'ngResource',
        'ui.bootstrap',
        'ui.select',
        'ui.router',
        'ui.grid',
        'ui.grid.edit',
        'ui.grid.rowEdit',
        'ui.grid.cellNav',
        'ui.grid.selection',
        'ui.grid.pinning',
        'ui.grid.autoResize',
        'ui.grid.resizeColumns',
        'xeditable',
        'smart-table',
        'ui.select',
        'permission',
        'permission.ui',
        'toastr',
        'ui.switchery',
        'angular-loading-bar',
        'daterangepicker',
        'angularMoment',
        'ui.calendar',
        'ui.grid.validate',
        'Gentelella.service',
        'Gentelella.commonservice',
        'Gentelella.signin',
        'Gentelella.sides',
        'Gentelella.components',
        'Gentelella.authService',
        'Gentelella.catalogo',
        'Gentelella.ventas'
    ])
    .constant('api', 'http://api-koofe.localhost')
    .config(['cfpLoadingBarProvider', '$urlRouterProvider', '$permissionProvider', '$compileProvider', 'toastrConfig', '$qProvider', 'uiSelectConfig', '$httpProvider', '$logProvider', '$resourceProvider', function(cfpLoadingBarProvider, $urlRouterProvider, $permissionProvider, $compileProvider, toastrConfig, $qProvider, uiSelectConfig, $httpProvider, $logProvider, $resourceProvider){
        $logProvider.debugEnabled(true);
        angular.lowercase = angular.$$lowercase;
        cfpLoadingBarProvider.includeSpinner = true;
        $urlRouterProvider.otherwise(function($injector, $location){
            return '/dashboard/home'
        });
        $permissionProvider.suppressUndefinedPermissionWarning(false);
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
        _.extend(toastrConfig, {allowHtml:true, closeHtml:'<button>&times;</button>'});
        $qProvider.errorOnUnhandledRejections(false);
        // configuracion.nameclass='nav-md';
        uiSelectConfig.theme = 'bootstrap';
        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage){
            return {
                'request': function(config){
                    config.headers=config.headers || {};
                    if($localStorage.token){
                        config.headers.Authorization='Bearer '+$localStorage.token;
                    }
                    return config;
                },
                'response':function(response){
                    if(response.data.token!==undefined){
                        $localStorage.token=response.data.token;}
                    return response;
                },
                'responseError': function(response){
                    if(response.status===401 || response.status==403 || response.status==402){
                        $location.path('/sigin');
                    }
                    return $q.reject(response);
                }
            }
        }]);
    }])
    .run(['$rootScope', '$state', 'AuthenticationService', function($rootScope, $state, AuthenticationService){
        $rootScope.bodyLayout='nav-md';

        $rootScope.$on('$stateChangeStart', function(evt, to, params, from){
            if(to.redirectTo){
                evt.preventDefault();
                $state.go(to.redirectTo, params, {location:'replace'});
            }
        });

        $rootScope.$on('$routeChangeSuccess', function(event, currentRoute){
        });
    }])
})();