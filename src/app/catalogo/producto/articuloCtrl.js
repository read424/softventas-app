(function(){
    'use strict';
    angular.module('Gentelella.catalogo.productos', [])
        .factory('Product', ['$resource', 'api', function($resource, api){
            return $resource(api+'/producto/:id', {'id':'@id'},
                {
                    'update':{
                        method:'PUT', isArray: false,
                        params:{ id: '@id'}
                    }
                }
            );
        }])
        .component('searchProductCodigo', {
            templateUrl: 'app/catalogo/producto/components/input-search-codigo.html',
            transclude: true,
            bindings: {
                model: '='
            },
            controller:['$scope', 'toastr', 'Product', function($scope, toastr, Product){
                $scope.getSearchProducto=function(search_text){
                    return Product.query({page:1, rows:10, s:search_text}).$promise.then(function(resp){
                         return _.map(resp, function(item){
                            return _.extend({}, {value: item.codigo, text: item.codigo}, _.pick(item, ['codigo', 'nombre', 'id', 'stock', 'precio']));
                        });
                    });    
                };

                $scope.selectedNombre=function($item, $model, $label, $event){
                    if(parseInt($item.stock)>0)
                        _.extend($scope.$ctrl.model, _.pick($item, ['id', 'nombre', 'stock', 'precio']));
                    else{
                        $scope.$ctrl.model.codigo='';
                        toastr.warning('El producto no contiene stock');
                        $event.originalEvent.preventDefault();
                    }
                };
            }]
        })
        .component('searchProductNombre', {
            templateUrl: 'app/catalogo/producto/components/input-search-nombre.html',
            transclude: true,
            bindings: {
                model: '='
            },
            controller: ['$scope', 'Product', 'toastr', function($scope, Product, toastr){
                $scope.getSearchProducto=function(search_text){
                    return Product.query({page:1, rows:10, s:search_text}).$promise.then(function(resp){
                         return _.map(resp, function(item){
                            return _.extend({}, {value: item.nombre, text: item.nombre}, _.pick(item, ['codigo', 'nombre', 'id', 'stock', 'precio']));
                        });
                    });    
                };

                $scope.selectedNombre=function($item, $model, $label, $event){
                    if(parseInt($item.stock)>0)
                        _.extend($scope.$ctrl.model, _.pick($item, ['id', 'codigo', 'stock', 'precio']));
                    else{
                        $scope.$ctrl.model.nombre='';
                        toastr.warning('El producto no contiene stock');
                        $event.originalEvent.preventDefault();
                    }
                };
            }]
        })
        .filter('propsFilter', [ function() {
            return function(items, props) {
                var out = [];
                if (angular.isArray(items)) {
                    var keys = Object.keys(props);
                    items.forEach(function(item) {
                        var itemMatches = false;
                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                            }
                        }
                        if (itemMatches)
                            out.push(item);
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }
                return out;
            };
        }])
        .directive('formProducto', function(){
            return {
                restrict: 'E',
                templateUrl: 'app/catalogo/producto/articulo-details.html',
            };
        })
        .directive('validationProducto', ['serviceAPI', function(serviceAPI){
            return {
                require : 'ngModel',
                transclude : true,
                scope :{ idProducto:'=', typeValidation:'='},
                link : function($scope, $element, $attrs, ngModel){
                    ngModel.$asyncValidators.validationProducto=function(modelValue, viewValue){
                        return serviceAPI.validarProducto({ value: viewValue, type:$scope.typeValidation }, $scope.idProducto).then(function(response){});
                    }
                }
            };
        }])
        .controller('ListproductoCtrl', ['$uibModalInstance', '$uibModal', '$state', '$scope', 'toastr', '$stateParams', 'Product', function( $uibModalInstance, $uibModal, $state, $scope, toastr, $stateParams, Product){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.rowCollection=[];

            _.extend($scope,
                {
                    tableState:false,
                    data:{
                        id_almacen:'', id_tipo_producto:'', tipo_producto:'', id_familia:'', tipo_familia:'', tipo_tela:'',
                        categoria_articulo: ''
                    }
                }
            );

            $scope.callServer=function(tableState){
                $scope.tableState = tableState;
                Product.query().$promise.then(function(resp){
                    $scope.displayed=resp;
                });

            };

            $scope.editar=function(id){
              $state.go('dashboard.catalogo.producto-details', {id_producto: id});
            };

            $scope.addProducto=function(){
              $state.go('dashboard.logistica.catalogo.articulo-details');
            };

            $scope.seleccionar_producto=function(id_producto){
              $uibModalInstance.close(id_producto);
            };

            $scope.generate_report=function(type){
            };

        }])
        .controller('articuloCtrl', [ '$timeout', '$filter', '$scope', 'toastr', '$uibModal', '$stateParams', '$uibModalInstance', 'Product', function($timeout, $filter, $scope, toastr, $uibModal, $stateParams, $uibModalInstance, Product){
            _.extend($scope, {
                data: {
                    id:0, descripcion: '', categoria: { value:'', descripcion:'Seleccionar...'}, referencia:'',  precio:'0',  peso:'0',  cnt_stock:'0', fec_ingreso:moment()
                },
                is_modal: false, datepicker: {}
            });
            $scope.is_modal=$stateParams.is_modal || false;

            $scope.datepicker={
                options:{
                    singleDatePicker:true,
                    locale:{
                        format:'DD/MM/YYYY',
                        daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                        monthNames: [
                            "Enero", "Febrero", "Marzo", "Abril",
                            "Mayo", "Junio", "Julio", "Agosto",
                            "Septiembre", "Octubre", "Noviembre", "Diciembre"
                        ]  
                    },
                    autoclose: true,
                    todayHighlight: true
                }
            };

            $scope.guardar=function(eForm){
                if(eForm.$invalid){
                    toastr.error("Vaya!!! el formulario require informacion para ser procesado")
                    return true;
                }
                if(_.isUndefined($scope.data.id) || $scope.data.id==0){
                    $scope.create(eForm);
                }else{
                    $scope.update(eForm);
                }
            };

            $scope.create=function(eForm){
                Product.save(_.omit($scope.data, ['id'])).$promise.then(function(resp){
                    _.extend($scope.data, {id:0, descripcion: '', categoria: { value:'', descripcion:'Seleccionar...'}, referencia:'',  precio:'0',  peso:'0',  cnt_stock:'0', fec_ingreso:moment()});
                    toastr.success('El registro fue creado con éxito');
                    eForm.$setPristine();
                    eForm.$setUntouched();
                }, function(resp){
                    toastr.error(resp.error)
                });
            };

            $scope.update=function(eForm){
                console.log('update')
                Product.update(_.omit($scope.data, ['cnt_stock'])).$promise.then(function(resp){
                    toastr.success('Los datos fueron guardados con éxito');
                    eForm.$setPristine();
                    eForm.$setUntouched();        
                }, function(resp){
                    toastr.error(resp.error)
                });
            };


            $scope.consultar=function(){
                if(_.isUndefined($stateParams.id_producto) || $stateParams.id_producto=='0')
                    return true;
                $scope.data.id=$stateParams.id_producto;
                Product.get(_.pick($scope.data, ['id'])).$promise.then(function(resp){
                    _.extend($scope.data, _.omit(resp, ['codigo', 'fec_registro', 'nombre', 'stock', 'id', 'categoria', 'id_categoria']));
                    $scope.data.categoria={value: resp.categoria.id, descripcion: resp.categoria.descripcion };
                    $scope.data.referencia=resp.codigo;
                    $scope.data.descripcion=resp.nombre;
                    $scope.data.cnt_stock=resp.stock;
                    $scope.data.fec_ingreso=resp.fec_registro;
                });
            };

            $scope.consultar();
        }]);
})();