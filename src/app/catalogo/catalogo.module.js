(function () {
  'use strict';
  angular.module('Gentelella.catalogo', [
    'Gentelella.catalogo.productos',
    'Gentelella.catalogo.categorias'
  ])
  .config(['$stateProvider', function ($stateProvider){
    $stateProvider.state('dashboard.catalogo', {
      url: '/productos',
      abstract: true,
      template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
      title: 'Catalogo',
      sidebarMeta: {
        icon: 'fa fa-cubes',
        order: 100
      }
    }).state('dashboard.catalogo.producto', {
      url: '/productos',
      templateUrl: 'app/catalogo/producto/list/list-articulos.html',
      controller: 'ListproductoCtrl',
      title: 'Productos',
      resolve: { $uibModalInstance: function () { return {}; } },
      sidebarMeta: {
        icon: 'fa fa-cubes',
        order: 100,
      }
    })
    .state('dashboard.catalogo.producto-details', {
      url: '/producto/details/:id_producto',
      templateUrl: 'app/catalogo/producto/win-articulo.html',
      controller: 'articuloCtrl',
      title:'Registro de Producto',
      params: { id_producto: '' },
      resolve:{ 
        $uibModalInstance:function(){ return {}; } 
      }
    }).state('dashboard.catalogo.categoria', {
      url: '/categorias',
      templateUrl: 'app/catalogo/categoria/list/list-categoria.html',
      controller: 'listcategoriaCtrl',
      title: 'Categorias',
      resolve: { $uibModalInstance: function () { return {}; } },
      sidebarMeta: {
        icon: 'fa fa-cubes',
        order: 200,
      }
    }).state('dashboard.catalogo.categoria-details', {
      url: '/categoria/details/:id_categoria',
      templateUrl: 'app/catalogo/categoria/win-categoria.html',
      controller: 'categoriaCtrl',
      title:'Registro de Categoria',
      params: { id_categoria: '' },
      resolve:{ 
        $uibModalInstance:function(){ return {}; } 
      }
    });
  }]);
})();
