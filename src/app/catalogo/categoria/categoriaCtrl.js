(function(){
    'use strict';
    angular.module('Gentelella.catalogo.categorias', [])
        .factory('Categoria', ['$resource', 'api', function($resource, api){
            return $resource(api+'/categoria/:id', { id:'@id' }, 
                {
                    'update':{
                        method: 'PUT', 
                        isArray: false,
                        params:{ 
                            id: '@id', 
                        }
                    }
                }
            );
        }])
        .directive('formCategoria', function(){
            return {
                restrict: 'E',
                templateUrl: 'app/catalogo/categoria/categoria-details.html',
            };
        })
        .directive('existDescripcion', ['$httpParamSerializerJQLike', '$q', 'serviceAPI', function($httpParamSerializerJQLike, $q, serviceAPI){
            return {
                require : 'ngModel',
                transclude : true,
                scope :{ idCategoria:'='},
                link : function($scope, $element, $attrs, ngModel){
                    ngModel.$asyncValidators.existDescripcion=function(modelValue, viewValue){
                        return serviceAPI.validarCategoria({ descripcion: viewValue }, $scope.idCategoria).then(function(response){});
                    }
                }
            };
        }])
        .component('selectCategoria', {
            templateUrl: 'app/catalogo/categoria/components/select-categoria.html',
            transclude: true,
            bindings:{
                model:'=',
                placeholder: '@'
            },
            controller:['$scope', 'Categoria', function($scope, Categoria){
                _.extend($scope, {
                    aCategoria:[{value: '', descripcion: 'Seleccionar...'}],
                    data:{
                        categoria:{value: '', descripcion: 'Seleccionar...'}
                    }
                });
                Categoria.query().$promise.then(function(resp){
                    $scope.aCategoria = _.union($scope.aCategoria, _.map(resp, function(item){
                        return {value: item.id, descripcion: item.descripcion };
                    }));
                });
            }]
        })
        .controller('listcategoriaCtrl', ['$uibModalInstance', '$uibModal', '$state', '$scope', 'toastr', '$stateParams', 'Categoria', function( $uibModalInstance, $uibModal, $state, $scope, toastr, $stateParams, Categoria){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.rowCollection=[];

            _.extend($scope,{
                    tableState:false,
                    change_tipo_producto:true,
                    data:{}
                }
            );

            $scope.seleccionar_articulo=function(id_articulo){
                $uibModalInstance.close(id_articulo);
            };

            $scope.applicar_filtro = function () {
                $scope.callServer($scope.tableState);
            };

            $scope.callServer=function(tableState){
                $scope.tableState = tableState;
                Categoria.query().$promise.then(function(resp){
                    console.log(resp)
                    $scope.displayed=resp;
                });
            };

            $scope.editar=function(id){
              $state.go('dashboard.catalogo.categoria-details', {id_categoria: id});
            };

            $scope.addProducto=function(){
              $state.go('dashboard.logistica.catalogo.articulo-details');
            };

            $scope.seleccionar_producto=function(id_producto){
              $uibModalInstance.close(id_producto);
            };

            $scope.changeStatus = function (index, idrow, statusrow) {
                $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    keyboard: false,
                    template: '<div class="modal-content"><div class="modal-header bg-primary"><i class="fa fa-info-circle"></i> <span>Confirmación</span><button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" ng-click="$dismiss()"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><span>Esta seguro de cambiar status a este registro?</span></div><div class="modal-footer"><button class="btn btn-primary" type="button" ng-click="$close()">Aceptar</button><button class="btn btn-default" type="button" ng-click="$dismiss()">Cerrar</button></div></div>'
                }).result.then(function () {
                    serviceAPI.changeStatusArticulo({ id: idrow, status: statusrow }).then(function (r) {
                        if (r.data.success && r.data.rows.status != statusrow) {
                            $scope.displayed[index].status = r.data.rows.status;
                            toastr.success('Los cambios se realizaron satisfactoriamente');
                        } else {
                            toastr.warning(r.data.message);
                        }
                    });
                });
            };

            $scope.generate_report=function(type){

            };

        }])
        .controller('categoriaCtrl', [ '$timeout', '$httpParamSerializerJQLike','$filter', '$scope', 'toastr', '$uibModal', '$stateParams', '$uibModalInstance', '$localStorage', 'Categoria', function($timeout, $httpParamSerializerJQLike, $filter, $scope, toastr, $uibModal, $stateParams, $uibModalInstance, $localStorage, Categoria){
            _.extend($scope, {
                data:{ id:0, descripcion:'' },
                is_modal:false
            });
            $scope.is_modal=$stateParams.is_modal || false;
            $scope.default=_.clone($scope.data);

            $scope.messages={ descripcion: { required: 'Campo obligatorio', descripcionExist: 'Pertenece a otro registro' }};


            $scope.guardar=function(eForm){
                if(_.isUndefined($scope.data.id) || $scope.data.id==0){
                    $scope.create(eForm);
                }else{
                    $scope.update(eForm);
                }
            };

            $scope.create=function(eForm){
                Categoria.save(_.extend({}, {id_categoria:$scope.data.id}, _.omit($scope.data, ['id']))).$promise.then(function(){
                    _.extend($scope.data, {id:0, descripcion:''});
                    toastr.success('El registro fue creado con éxito');
                    eForm.$setPristine();
                    eForm.$setUntouched();        
                });
            };

            $scope.update=function(eForm){
                Categoria.update($scope.data).$promise.then(function(resp){
                    toastr.success('Los datos fueron guardados con éxito');
                    eForm.$setPristine();
                    eForm.$setUntouched();        
                });
            };

            $scope.consultar=function(){
                if(_.isUndefined($stateParams.id_categoria) || _.isEmpty($stateParams.id_categoria))
                    return true;
                $scope.data.id=$stateParams.id_categoria;
                Categoria.get(_.pick($scope.data, ['id'])).$promise.then(function(resp){
                    _.extend($scope.data, _.pick(resp, ['descripcion', 'id']));
                }, function(response){
                });
            };

            $scope.consultar();
        }]);
})();