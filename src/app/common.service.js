(function() {
    'use strict';
    angular.module('Gentelella.commonservice', [])
        .factory('permissions', ['AuthenticationService', function(AuthenticationService){
            return{
                hasPermissions:function(state){
                    var permissionList=_.union(AuthenticationService.getStatesusers(), []);//'dashboard.almacenes.reportes.kardex'
                    if(!angular.isString(state)){
                        return false;
                    }
                    return(permissionList.indexOf(state)===-1);
                }
            }
        }])
        .directive('hasPermissions', ['permissions', function(permissions){
            return{
                scope:{
                    name:'=namestate'
                },
                link:function(scope, element, attrs){
                    element[0].style.display=(permissions.hasPermissions(scope.name))?'none':'block';
                }
            }
        }])
        .directive('chartDashboard', ['AuthenticationService', function(AuthenticationService){
            return{
                scope:{
                    name:'=namestate'
                },
                link:function(scope, element, attrs){
                    var a_permissions=AuthenticationService.getPermissions();
                    element[0].style.display=(!a_permissions.includes(scope.name))?'none':'block';
                }
            }
        }])
        .directive('isValidStock', [function(){
            return {
                scope:{articulosSinStock:'='},
                link:function(scope, elm, attrs, ctrl){
                    ctrl.$validators.isValidStock=function(modelValue, viewValue){
                        return (scope.articulosSinStock.length==0);//( || );
                    }
                }
            }
        }])
        .directive('formatHora', [ function(){
            return {
                require:'ngModel',
                link:function(scope, elm, attrs, ctrl){
                    ctrl.$validators.formatInput=function(modelValue, viewValue){
                        if(ctrl.$isEmpty(modelValue))
                            return true;
                        console.log(/\d{1,2}:\d{2}/.test(viewValue))
                        if(/\d{1,2}:\d{2}/.test(viewValue)){
                            return true;
                        }
                        return false;
                    }
                }
            }
        }])
        .factory('commonService',commonService);

        
    /** @ngInject */
    function commonService($http, $timeout) {
        var service = {};
        var data = '';

        service.set = function(content) {
            data = content;
        };

        service.get = function() {
            return data;
        };

        return service;
    }
})();