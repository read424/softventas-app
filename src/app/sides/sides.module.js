(function(){
    'use strict';
    angular.module('Gentelella.sides',[ 'Gentelella.home.controller'])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('dashboard', {
                    url:'/dashboard',
                    templateUrl: 'app/sides/sides.html',
                    redirectTo:'home'
                })
                .state('dashboard.home', {
                    url:'/home',
                    templateUrl:'app/sides/home.html',
                    controller: 'homeCtrl',
                });
        }])
})();