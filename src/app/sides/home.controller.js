(function(){
    'use strict';
    angular.module('Gentelella.home.controller',[])
    .controller('homeCtrl', [ '$rootScope', '$scope', 'serviceAPI', function($rootScope, $scope, serviceAPI){
        $rootScope.bodyLayout='nav-md';
        $scope.locale_picker={
            format:'DD/MM/YYYY',
            daysOfWeek:[ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            monthNames: [
                "Enero", "Febrero", "Marzo", "Abril",
                "Mayo", "Junio", "Julio", "Agosto",
                "Septiembre", "Octubre", "Noviembre", "Diciembre"
            ]
        };
    }])
})();