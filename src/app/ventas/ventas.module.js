(function () {
  'use strict';
  angular.module('Gentelella.ventas', [
    'Gentelella.ventas.directas'
  ])
  .config(['$stateProvider', function ($stateProvider){
    $stateProvider.state('dashboard.ventas', {
      url: '/ventas',
      template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
      title: 'Ventas',
      sidebarMeta: {
        icon: 'fa fa-calculator',
        order: 200
      }
    })
    .state('dashboard.ventas.pos', {
      url: '/market',
      templateUrl: 'app/ventas/pos/win-ventas.html',
      controller: 'gridventasCtrl',
      title: 'Directas',
      resolve: { $uibModalInstance: function () { return {}; } },
      sidebarMeta: {
        icon: 'fa fa-calculator',
        order: 200,
      }
    });
  }]);
})();
