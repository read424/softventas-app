(function() {
  'use strict';
  angular.module('Gentelella.ventas.directas', [])
    .factory('eShop', ['$resource', 'api', function($resource, api){
      return $resource(api+'/eshop/:id', {id:'@id'});
    }])
    .directive('formVentas', function(){
      return {
        restrict: 'E',
        templateUrl: 'app/ventas/pos/form_ventas.html',  
      }
    })
    .controller('gridventasCtrl', ['$uibModalInstance', '$scope', '$state', 'toastr', '$uibModal', '$stateParams', '$q', '$filter', '$timeout', 'uiGridConstants', 'uiGridValidateService', '$window', 'eShop', function($uibModalInstance, $scope, $state, toastr, $uibModal, $stateParams, $q, $filter, $timeout, uiGridConstants, uiGridValidateService, $window, eShop) {
      uiGridValidateService.setValidator('validarcantidad', function(argument){
        return function(oldValue, newValue, rowEntity, colDef){
          if(isNaN(newValue)){
            return false;
          }
          return true;
        };
      }, function(argument){
        return 'You can only insert names starting with: "' + argument + '"';
      });

      uiGridValidateService.setValidator('validarsaldo', function(argument) {
        return function(oldValue, newValue, rowEntity, colDef) {
          if(parseInt(rowEntity.stock)==0 || newValue>parseInt(rowEntity.stock)){
            return false;
          }
          return true;
          //return false;//devolver false para invalidar el monto asignado
        };
      }, function(argument) {
        return 'You can only insert names starting with: "' + argument + '"';
      });

      _.extend($scope, {
        gridApi:false, 
        data: {
          cantidad:0,
          producto:{ id:0, codigo:'', nombre:'', stock:0, precio:0 }
        }, 
        gridOptions:{
          showGridFooter: false,
          showColumnFooter: false,
          enableRowSelection: true,
          enableSelectAll: true,
          multiSelect:true,
          enableRowHeaderSelection: true,
          enableCellEdit: true,
          enableGridMenu:false,
          enableColumnMenus: false,
          data:[],
          onRegisterApi:function(gridApi){
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);

            gridApi.validate.on.validationFailed($scope, function(rowEntity, colDef, newValue, oldValue){
              if(isNaN(newValue) || _.isEmpty(newValue) || parseInt(newValue)>parseInt(rowEntity.stock) )
                toastr.error("La cantidad ingresa no es válida");
            });

            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
              var _index = $scope.gridOptions.data.indexOf(rowEntity);
              if(colDef.name=='cantidad'){
                if(!isNaN(newValue) && newValue>0)
                  $scope.gridOptions.data[_index].total=parseInt(rowEntity.precio)*parseInt(newValue);
              }
              $scope.$apply();
            });
            $scope.gridApi.grid.registerRowsProcessor($scope.hiddenRow, 200 );
          },
          columnDefs:[
            { name: 'id_articulo', visible:false },
            { name: 'stock', visible:false },
            {
              name: 'accion', displayName:'', width: '4%', enableCellEdit: false, 
              cellTemplate: '<a class="btn btn-link" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash text-danger"></i></a>'
            },
            { name: 'codigo', displayName:'Código', width: '10%', enableCellEdit: false, enableCellEditOnFocus:false, 
              cellClass:function(grid, row, col, rowRenderIndex, colRenderIndex){
                return (parseFloat(row.entity.cantidad_disponible)==0.00)?'':'';
              }
            },
            { name: 'nombre', displayName: 'Descripción' , width: '29%', enableCellEdit: false, enableCellEditOnFocus:false, 
              cellClass:function(grid, row, col, rowRenderIndex, colRenderIndex){
                return (row.entity.cantidad_disponible!=row.entity.cantidad)?'':'';
              }
            },
            { name: 'precio', displayName: 'Precio',  width: '10%', cellClass:'text-right', enableCellEdit: false},
            { name: 'cantidad', headerCellClass:'text-right', displayName: 'Cantidad', width: '8%', cellClass:'text-right', enableCellEdit: true, enableCellEditOnFocus:true, visible:true, cellFilter: 'number: 2', cellTemplate: 'ui-grid/cellTitleValidator',
              validators: {required: true, validarsaldo: true, validarcantidad: true}
            },
            { name: 'total', headerCellClass:'text-right', displayName: 'Total', width: '8%', cellClass:'text-right', enableCellEdit: false, enableCellEditOnFocus:false, visible:true, cellFilter: 'number: 2'},
            { name: 'delete', visible:false },
          ]
        }
      });

      $scope.guardar=function(eForm){
        eForm.$pending=true;
        eShop.save({data:$scope.gridOptions.data}).$promise.then(function(response){
          _.extend($scope.data.producto, {id:0, nombre:'', codigo:'', precio:0, stock:0});
          $scope.data.cantidad=0;
          eForm.$pending=false;
          eForm.$setPristine();
          eForm.$setUntouched();
        }, function(msg){
          eForm.$pending=false;
          toastr.error(msg.error, 'Error');
        });
      };

      $scope.hiddenRow = function (renderableRows) {
        renderableRows.forEach(function (row) {
            if (row.entity.delete == '1') { row.visible = false; }
        });
        return renderableRows;
      };

      $scope.deleteRow = function(row){
        var index = $scope.gridOptions.data.indexOf(row.entity);
        if(!_.isUndefined(row.entity.id_detordeningreso) && !isNaN(row.entity.id_detordeningreso) && !_.isEmpty(row.entity.id_detordeningreso)){
          $scope.gridOptions.data[index].delete='1';
          $scope.gridApi.grid.refresh();
        }else{//es temporal puede borrarse 
          $scope.gridOptions.data.splice(index, 1);
        }
      };

      $scope.addItems=function(){
        if(!_.isUndefined(_.findWhere($scope.gridOptions.data, {id:$scope.data.id_articulo}))){
          toastr.error("El Producto se encuentra registrado, intente cambiar la cantidad", "error");
          return true;
        }
        var item=_.extend({}, 
          _.pick($scope.data.producto, [ 'id', 'codigo', 'nombre', 'precio', 'stock']),
          _.pick($scope.data, ['cantidad']), {total: parseInt($scope.data.cantidad)*parseInt($scope.data.producto.precio)}
        );
        $scope.gridOptions.data.push(item);
        _.extend($scope.data.producto, {id:0, codigo: '', nombre:'', precio:0, cantidad:0});
        $scope.data.cantidad=0;
      };

      $scope.saveRow=function(rowEntity){
        var promise = $q.defer();
        $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise);
        promise.resolve();
      };
    }]);
})();