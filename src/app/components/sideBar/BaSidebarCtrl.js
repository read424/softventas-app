(function(){
  'use strict';
  angular.module('Gentelella.components')
      .provider('baSidebarService', [function(){
        this.$get=function($state){
          return new _factory();
          function _factory(){
            this.getMenuItems = function() {
              var states = defineMenuItemStates();
              var menuItems = states.filter(function(item) {
                return item.level == 0;
              });
              menuItems.forEach(function(item) {
                var children = states.filter(function(child) {
                  return child.level == 1 && child.name.indexOf(item.name) === 0;
                });
                children.forEach(function(item) {
                  var children = states.filter(function(child) {
                    return child.level == 2 && child.name.indexOf(item.name) === 0;
                  });
                  item.subMenu = children.length ? children : null;
              });
                item.subMenu = children.length ? children : null;
                
              });
              return menuItems;
            };

            function defineMenuItemStates() {
              return $state.get()
                  .filter(function(s) {
                    return s.sidebarMeta;
                  })
                  .map(function(s) {
                    var meta = s.sidebarMeta;
                    return {
                      name: s.name.substr(10),
                      title: s.title,
                      level: (s.name.match(/\./g) || []).length -1,
                      order: meta.order,
                      icon: meta.icon,
                      stateRef: s.name,
                    };
                  })
                  .sort(function(a, b) {
                    return (a.level - b.level) * 100 + a.order - b.order;
                  });
            }; 
          }
        };
      }])
      .controller('BaSidebarCtrl', ['$scope', 'AuthenticationService', 'baSidebarService', '$localStorage', function($scope, AuthenticationService, baSidebarService, $localStorage){

        $scope.notification_array=$localStorage.notification_array || [];
        $scope.num_notification=$scope.notification_array.length;

        $scope.empleado = AuthenticationService.getEmpleado();
        $scope.menuItems = baSidebarService.getMenuItems();
        // $scope.defaultSidebarState = (!_.isUndefined($scope.menuItems[0]))?$scope.menuItems[0].stateRef:[];
        $scope.area = AuthenticationService.getArea();

      }]);
})();
