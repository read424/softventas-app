(function(){
    'use strict';
    angular.module('Gentelella.components')
        .directive('sideFooter', [function(){
            return {
                restrict: 'E',
                controller:['$scope', function($scope){
                }],
                templateUrl: 'app/components/sideFooter/sideFooter.html'
            };
        }])
})();