(function(){
    'use strict';
    angular.module('Gentelella.components')
        .directive('formChangePassword', function(){
            return {
                restrict: 'E',
                templateUrl: 'app/usuarios/assigned_password.html'
            };
        })
        .directive('topNavigation', [function(){
            return {
                restrict: 'E',
                templateUrl: 'app/components/topNavigation/topNavigation.html',
                controller: 'SignOutCtrl',
                link: function($scope) {
                    var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
                    $BODY = $('body'),
                    $MENU_TOGGLE = $('#menu_toggle'),
                    $SIDEBAR_MENU = $('#sidebar-menu'),
                    $SIDEBAR_FOOTER = $('.sidebar-footer'),
                    $LEFT_COL = $('.left_col'),
                    $RIGHT_COL = $('.right_col'),
                    $NAV_MENU = $('.nav_menu'),
                    $FOOTER = $('footer');
                    // Sidebar
                    $(document).ready(function() {
                        // TODO: This is some kind of easy fix, maybe we can improve this
                        var setContentHeight = function () {
                            // reset height
                            $RIGHT_COL.css('min-height', $(window).height());
                    
                            var bodyHeight = $BODY.outerHeight(),
                                footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                                leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                                contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
                    
                            // normalize content
                            contentHeight -= $NAV_MENU.height() + footerHeight;
                    
                            $RIGHT_COL.css('min-height', contentHeight);
                        };
                        // toggle small or large menu
                        $MENU_TOGGLE.on('click', function() {
                            if ($BODY.hasClass('nav-md')) {
                                $SIDEBAR_MENU.find('li.active ul').hide();
                                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                            } else {
                                $SIDEBAR_MENU.find('li.active-sm ul').show();
                                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                            }
                    
                            $BODY.toggleClass('nav-md nav-sm');
                    
                            setContentHeight();
                    
                            $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
                        });
                        // Tooltip
                        $('[data-toggle="tooltip"]').tooltip({
                            container: 'body'
                        });
                        // Progressbar
                        if ($(".progress .progress-bar")[0]) {
                            $('.progress .progress-bar').progressbar();
                        }
                    });
                }
            };
        }])
        .directive('validCurrentPassword', [ '$q', 'serviceAPI', function($q, serviceAPI){
            return {
                require : 'ngModel',
                transclude : true,
                scope :{},
                link : function($scope, $element, $attrs, ngModel){
                    ngModel.$asyncValidators.validCurrentPassword=function(modelValue, viewValue){
                        if(ngModel.$isEmpty(modelValue))
                            return $q.reject();
                        return serviceAPI.validatePassword({contrasenia:modelValue}).then(function(r){
                            if(r.data.success && r.data.num_rows==1)
                                return $q.resolve();
                            return $q.reject();
                        });
                    }
                }
            }
        }])
        .controller('SignOutCtrl', ['$scope', 'AuthenticationService', '$state', 'PermPermissionStore', '$uibModal', function($scope, AuthenticationService, $state, PermPermissionStore, $uibModal){
            PermPermissionStore.clearStore();
            $scope.username = AuthenticationService.getUser();
            $scope.signOut = function(){
                AuthenticationService.setLoggedIn(false);
                AuthenticationService.signOut();
                $state.go('signin');
            };

            $scope.change_password=function(){
                try{
                    $uibModal.open({
                        animation: true,
                        backdrop: 'static',
                        windowClass: 'win_change_password',
                        controller:'resetPasswordCtrl',
                        template: '<div class="modal-content"><div class="modal-header bg-primary"><i class="fa fa-info-circle"></i> <span>Cambiar contrase&ntilde;a</span><button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" ng-click="$dismiss()"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><form-change-password></form-change-password></div><div class="modal-footer"><button class="btn btn-primary" ng-disabled="formChangePassword.$invalid" type="button" ng-click="assigned_new_password(formChangePassword)">Aceptar</button><button class="btn btn-default" type="button" ng-click="$dismiss()">Cerrar</button></div></div>'
                    }).result.then(function (response) {
                    
                    });    
                }catch(e){
                    console.log(e)
                }
            };

            $scope.open_qr_code=function(){
                $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    transclude: true,
                    windowClass: 'win_read_qr',
                    controller:['$scope', 'socketFactory', '$uibModalInstance', 'serviceAPI', function($scope, socketFactory, $uibModalInstance, serviceAPI){
                        $scope.codigo_qr='AQUI VA EL CODIGO QR';
                        $scope.socket = {};
                        _.extend($scope, {
                            data: {
                                image_dataurl:''
                            }
                        });
                        $scope.socket = socketFactory({
                            ioSocket: io.connect('http://127.0.0.1:3030/')
                        });

                        $scope.socket.on('connect', function (){
                            $scope.socket.emit('assigned_channel', 'bot-whatsapp-get-qr');
                            serviceAPI.getQrBotWhatsapp({}).then(function(response){

                            });
                        });
                        
                        $scope.socket.on('assigned_channel', function(message){
                            console.log('assigned_channel: ', message);
                        });

                        $scope.socket.on('qr', function(data){
                            $scope.data.image_dataurl=data.qr;
                        });

                        $scope.socket.on('success_qr', function(data){
                            $scope.socket.emit('discconnect')
                            console.log(data);
                            $uibModalInstance.close();
                        });

                        $scope.socket.on('connect_error', function (){
                            console.log('connect_error')
                        });

                        $scope.socket.on('discconnect', function (){
                            console.log('discconnect')
                        });

                        $scope.socket.on('connection', function(socket){
                            console.log('connection')
                        });

                    }],
                    template: '<div class="modal-content"><div class="modal-header bg-primary"><i class="fa fa-info-circle"></i> <span>Cambiar contrase&ntilde;a</span><button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" ng-click="$dismiss()"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><img ng-if="data.image_dataurl!=\'\'" ng-src="{{data.image_dataurl}}" /></div><div class="modal-footer"><button class="btn btn-primary" ng-disabled="formChangePassword.$invalid" type="button" ng-click="assigned_new_password(formChangePassword)">Aceptar</button><button class="btn btn-default" type="button" ng-click="$dismiss()">Cerrar</button></div></div>'
                }).result.then(function (response) {
                    console.log($scope.socket)
                });    
            };

        }])
        .controller('resetPasswordCtrl', ['$scope', 'serviceAPI', '$uibModalInstance', 'toastr', function($scope, serviceAPI, $uibModalInstance, toastr){
            _.extend($scope, {
                data: {
                    current_password:'', new_password:'', repeat_password:''
                }
            });

            $scope.assigned_new_password=function(form){
                serviceAPI.assignedPassword($scope.data).then(function(r){
                    var typenotify=(r.data.success)?'warning':'error';
                    if(r.data.success && r.data.affected_rows==1){
                        typenotify='success';
                        $uibModalInstance.close();
                    }
                    toastr[typenotify](r.data.message);
                });
            }
        }]);
})();