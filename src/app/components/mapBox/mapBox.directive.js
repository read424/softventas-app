(function(){
    'use strict';
    angular.module('Gentelella.components')
        .directive('mapBox', [ function(){
            return {
                restrict:'E',
                template: '<div style="height:300px;" id="container-map"></div><pre id="info"></pre>',
                link:function(scope, attrs){
                    mapboxgl.accessToken='pk.eyJ1IjoicmVhZDQyNCIsImEiOiJjanA0aDg5cjcwY3pkM3ZuOHh4azFhdm9mIn0.E5rGcI6XrxXHbryxXodZlw';
                    var map = new mapboxgl.Map({
                        container: 'container-map',
                        style: 'mapbox://styles/mapbox/streets-v9',
                        zoom:6,
                        center:[-55.950233, -32.374290]
                    }).addControl(new mapboxgl.NavigationControl());
                    var marker= new mapboxgl.Marker({draggable:true}).setLngLat([-55.950233, -32.374290]).addTo(map);
                    marker.on('dragend', function(){
                        console.log(marker.getLngLat())
                    });
        
                }
            }
        }]);
})();