(function(){
    'use strict';
    angular.module('Gentelella.service',[])
        .service('SocketService', ['socketFactory', 'api_node', function(socketFactory, api_node){
            return socketFactory({ 
                ioSocket: io.connect(api_node) 
            });
        }])
        .factory('body', [function(){
            return {
                sharedObject:{ data:'test-shared'}
            }
        }])
        .factory('serviceAPI', [ '$http', 'api', function($http, api){
            return {
                validarCategoria:function(data, id){ return $http.post(api+'/validar/categoria/'+id, data); },
                validarProducto:function(data, id){ return $http.post(api+'/validar/producto/'+id, data); },
            }
        }]);
})();