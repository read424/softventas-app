(function(){
    'use strict';
    angular.module('Gentelella.signin', [
        'Gentelella.signin.controller'
    ])
    .config(['$stateProvider', function($stateProvider){
        $stateProvider
            .state('signin', {
                url:'/signin',
                controller: 'SigninCtrl',
                templateUrl: 'app/signin/signin.html'
            })
    }])
})();